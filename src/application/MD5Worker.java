/*
==============================================================================================================================================================

www.sourceforge.net/projects/md5multichecker
Copyright Stephane Baudoux (www.stephane-baudoux.com)

This file is part of MD5 Multi-Checker.

SiniP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MD5 Multi-Checker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MD5 Multi-Checker.  If not, see <http://www.gnu.org/licenses/>.

==============================================================================================================================================================
*/

package application;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import tools.JTableElement;

/**
 * 
 * @author Stephane Baudoux
 * MD5 implementation of the AbstractWorker class
 *
 */
public class MD5Worker extends AbstractWorker {
	
	/**
	 * Constructor
	 * @param c The controller
	 */
	public MD5Worker(Controller c) {
		
		controller = c;
		
		digest = null;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {}
	}
	
	/**
	 * Get the file checksum
	 * @param f The file
	 * @return The checksum
	 * @throws Exception
	 */
	private byte[] getCheckSum(File f) throws Exception {
		
		FileInputStream fis = new FileInputStream(f);
		digest.reset();
		
		int numRead;
		int fileProgression = 0;
	
		/**
		 * Read the file by chunks to avoid using too much ram
		 */
		do {
	       numRead = fis.read(buffer);
	       fileProgression += numRead;
	       if(numRead > 0) {
	    	   digest.update(buffer, 0, numRead);
	    	   
	    	   /**
				 * Progression update
				 */
				controller.onPendingWorkChange((int)(progressionStep * doneWork.size() + (fileProgression / (float)f.length()) * progressionStep));
	       }
		} while (numRead != -1 && !canceled);
	
		fis.close();
		
		/**
		 * Stopped by the user
		 */
		if(canceled)
			throw new Exception();
		
		return digest.digest();
	}
	
	/**
	 * Find the MD5 of the first pending file in the array
	 */
	protected void performFile() {
		
		File f = pendingWork.get(0);
		byte[] checksum;
		
		try {
			checksum = getCheckSum(f);
			
			/**
			 * Get the hexadecimal
			 */
			StringBuilder stringBuilder = new StringBuilder();
			for(int i = 0 ; i < checksum.length ; i++) {
				String hex = Integer.toHexString(checksum[i]);
				if(hex.length() == 1) {
					stringBuilder.append('0');
					stringBuilder.append(hex.charAt(hex.length() - 1));
				}
				else
					stringBuilder.append(hex.substring(hex.length() - 2));
			}
			
			/**
			 * Remove only if the process has been stopped by the user
			 */
			synchronized(pendingWork)
			{
				if(pendingWork.size() > 0)
					pendingWork.remove(0);
			}
			
			doneWork.add(f);
			
			/**
			 * Send the element
			 */
			controller.onNewAddedElement(new JTableElement(f.getAbsolutePath(), stringBuilder.toString()));
			
			/**
			 * Progression update
			 */
			controller.onPendingWorkChange((int)(progressionStep * doneWork.size()));
		} catch (Exception e) {}
	}
	
	@Override
	public void run() {
		
		while(alive) {
			if(pendingWork.size() > 0)
				performFile();
			else {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {}
			}
		}
	}
}
