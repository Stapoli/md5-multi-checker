/*
==============================================================================================================================================================

www.sourceforge.net/projects/md5multichecker
Copyright Stephane Baudoux (www.stephane-baudoux.com)

This file is part of MD5 Multi-Checker.

SiniP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MD5 Multi-Checker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MD5 Multi-Checker.  If not, see <http://www.gnu.org/licenses/>.

==============================================================================================================================================================
*/

package application;

import java.awt.EventQueue;
import java.io.File;

import tools.LangManager;

/**
 * 
 * @author Stephane Baudoux
 * The controller.
 * Do the link between the model and the view
 *
 */
public class Controller {
	
	/**
	 * The view
	 */
	private View view;
	
	/**
	 * The worker
	 */
	private AbstractWorker worker;
	
	/**
	 * Constructor
	 */
	public Controller() {
		
		create();
		initialize();
	}
	
	/**
	 * Create the elements
	 */
	public void create() {
		
		view = new View();
		worker = new MD5Worker(this);
		new Thread(worker).start();
	}
	
	/**
	 * Initialize the elements
	 */
	public void initialize() {
		
		view.setController(this);
		view.setVisible(true);
	}
	
	/**
	 * Called when a new file need to be added
	 * @param file The file name
	 */
	public void onNewAddedFile(final String file) {
		
		if(!worker.hasWork())
			onPendingWorkStart();
		
		new Thread(new Runnable()  {
		      public void run() {
		    	  worker.addWork(new File(file), view.isRecursiveCallChecked());
		      }
		}).start();
	}
	
	/**
	 * Called when a new element need to be added to the table
	 * @param e The Table element
	 */
	public void onNewAddedElement(Object e) {
		
		final Object element = e;
		EventQueue.invokeLater(new Runnable() {
          public void run() {
        	  view.addElement(element);
          }
        });
		
		if(!worker.hasWork())
  		  onPendingWorkDone();
	}
	
	/**
	 * Called when a new pending work starts
	 */
	public void onPendingWorkStart() {
		
		worker.setCanceled(false);
        EventQueue.invokeLater(new Runnable() {
          public void run() {
        	  view.addSubtitle(LangManager.getInstance().getLangText("hashInProgress"));
        	  view.setInProgress(true);
          }
        });
	}
	
	/**
	 * Called when all the pending work is done
	 */
	public void onPendingWorkDone() {
		
		worker.clearWork();
        EventQueue.invokeLater(new Runnable() {
          public void run() {
        	  view.removeSubtitle();
        	  view.setInProgress(false);
        	  view.setProgression(0);
          }
        });
	}
	
	/**
	 * Called when the pending work is canceled
	 */
	public void onPendingWorkCanceled() {
		
		worker.setCanceled(true);
		onPendingWorkDone();
	}
	
	/**
	 * Called when the progression change
	 * @param progression The progression
	 */
	public void onPendingWorkChange(int progression) {
		
		view.setProgression(progression);
	}
	
	/**
	 * Called when the program is exited
	 */
	public void onProgramExit() {
		
		worker.setAlive(false);
	}
}
