/*
==============================================================================================================================================================

www.sourceforge.net/projects/md5multichecker
Copyright Stephane Baudoux (www.stephane-baudoux.com)

This file is part of MD5 Multi-Checker.

SiniP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MD5 Multi-Checker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MD5 Multi-Checker.  If not, see <http://www.gnu.org/licenses/>.

==============================================================================================================================================================
*/

package application;

import java.io.File;
import java.security.MessageDigest;
import java.util.ArrayList;

/**
 * The abstract worker class.
 * Describe the basic function of the needed worker
 * @author Stephane Baudoux
 *
 */
public abstract class AbstractWorker implements Runnable {
	
	/**
	 * The thread alive status
	 */
	protected boolean alive;
	
	/**
	 * The canceled status
	 */
	protected boolean canceled;
	
	/**
	 * The pending work
	 */
	protected ArrayList<File> pendingWork;
	
	/**
	 * The done work
	 */
	protected ArrayList<File> doneWork;
	
	/**
	 * The message digest
	 */
	protected MessageDigest digest;
	
	/**
	 * The checksum calculation buffer
	 */
	protected byte[] buffer;
	
	/**
	 * The progression step
	 */
	protected float progressionStep;
	
	/**
	 * The controller
	 */
	protected Controller controller;
	
	/**
	 * Constructor
	 */
	public AbstractWorker() {
		
		alive = true;
		canceled = false;
		pendingWork = new ArrayList<File>();
		doneWork = new ArrayList<File>();
		buffer = new byte[2048];
	}
	
	/**
	 * Add a work
	 * @param file The file to add
	 */
	public void addWork(File file, boolean recursive) {
		
		/**
		 * If it's a directory and the recursivity is enabled, look into it
		 */
		if(file.isDirectory()) {
			File[] fileList;
			fileList = file.listFiles();
			
			for(int i = 0 ; i < fileList.length ; i++) {
				if(fileList[i].isFile() || (fileList[i].isDirectory() && recursive))
					addWork(fileList[i], recursive);
			}
		}
		else {
			/**
			 * It's a file, it's added to the list and the progression step is updated
			 */
			synchronized(pendingWork)
			{
				pendingWork.add(file);
				progressionStep = 100.0f / (pendingWork.size() + doneWork.size());
			}
		}
	}
	
	/**
	 * Perform a task for the first file in the pending list
	 */
	protected abstract void performFile();
	
	/**
	 * Set the alive status
	 * @param alive The alive status
	 */
	public void setAlive(boolean alive) {
		
		this.alive = alive;
	}
	
	/**
	 * Set the canceled status
	 * @param canceled The canceled status
	 */
	public void setCanceled(boolean canceled) {
		
		this.canceled = canceled;
	}
	
	/**
	 * Remove all work files
	 */
	public void clearWork() {
		
		pendingWork.clear();
		doneWork.clear();
	}
	
	/**
	 * Check if the worker has work to do
	 * @return True if the worker has work
	 */
	public boolean hasWork() {
		
		return pendingWork.size() > 0;
	}

	@Override
	public abstract void run();
}
