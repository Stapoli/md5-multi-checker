/*
==============================================================================================================================================================

www.sourceforge.net/projects/md5multichecker
Copyright Stephane Baudoux (www.stephane-baudoux.com)

This file is part of MD5 Multi-Checker.

SiniP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MD5 Multi-Checker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MD5 Multi-Checker.  If not, see <http://www.gnu.org/licenses/>.

==============================================================================================================================================================
*/

package application;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import tools.ColorCellRenderer;
import tools.JTableElement;
import tools.JTableModel;
import tools.LangManager;
import tools.OptionsManager;

/**
 * 
 * @author Stephane Baudoux
 * The program view
 *
 */
public class View extends JFrame implements ActionListener, DropTargetListener, ListSelectionListener, WindowListener, ComponentListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The program title
	 */
	private static final String programTitle = "MD5 Multi-Checker";
	
	/**
	 * The program version
	 */
	private static final String programVersion = "1.0";
	
	/**
	 * The program resources directory
	 */
	private static final String iconsDirectory = "resources/icons/";
	
	/**
	 * The program resources directory
	 */
	private static final String logoDirectory = "resources/logo/";
	
	/**
	 * The default view width
	 */
	private static final int WINDOW_WIDTH = 650;
	
	/**
	 * The default view height
	 */
	private static final int WINDOW_HEIGHT = 458;
	
	/**
	 * The initial table size
	 */
	private static final int INITIAL_HASH_SIZE = 19;
	
	/**
	 * The starting value of the current selection 
	 */
	private int selectionStart;
	
	/**
	 * The ending value of the current selection
	 */
	private int selectionEnd;
	
	/**
	 * The language manager
	 */
	private LangManager langM = LangManager.getInstance();
	
	/**
	 * The options manager
	 */
	private OptionsManager optM = OptionsManager.getInstance();
	
	/**
	 * The controller
	 */
	private Controller controller;
	
	/**
	 * The header panel
	 */
	private JPanel header;
	
	/**
	 * The header new list button
	 */
	private JButton headerNewListButton;
	
	/**
	 * The header add files button
	 */
	private JButton headerAddButton;
	
	/**
	 * The header add directories button
	 */
	private JButton headerAddDirectoryButton;
	
	/**
	 * The header remove button
	 */
	private JButton headerRemoveButton;
	
	/**
	 * The footer panel
	 */
	private JPanel footer;
	
	/**
	 * The footer progress bar
	 */
	private JProgressBar footerProgressBar;
	
	/**
	 * The footer stop button
	 */
	private JButton footerStopButton;
	
	/**
	 * The hash table
	 */
	private JTable hashTable;
	
	/**
	 * The hash table model
	 */
	private JTableModel hashModel;
	
	/**
	 * The scroll pane
	 */
	private JScrollPane hashScrollPane;
	
	/**
	 * The menu item new list button
	 */
	private JMenuItem newListMenuItem;
	
	/**
	 * The menu item add files button
	 */
	private JMenuItem addFilesMenuItem;
	
	/**
	 * The menu item add directories button
	 */
	private JMenuItem addDirectoryMenuItem;
	
	/**
	 * The menu item exit button
	 */
	private JMenuItem quitMenuItem;
	
	/**
	 * The menu item exit button
	 */
	private JMenuItem aboutMenuItem;
	
	/**
	 * The menu item recrusivity button
	 */
	private JCheckBoxMenuItem recursiveCallBox;
	
	/**
	 * The file chooser
	 */
	private JFileChooser fileChooser;
	
	/**
	 * Constructor
	 */
	public View() {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e) {}
		
		create();
		initialize();
		addListeners();
	}
	
	/**
	 * Set the controller
	 * @param c The controller
	 */
	public void setController(Controller c) {
		
		controller = c;
	}
	
	/**
	 * Create the elements
	 */
	public void create() {
		
		/**
		 * Menu items
		 */
		newListMenuItem = new JMenuItem(langM.getLangText("menuBarNewListMenuItem"), new ImageIcon(iconsDirectory + "newListSmall.png"));
		addFilesMenuItem = new JMenuItem(langM.getLangText("menuBarAddFilesMenuItem"), new ImageIcon(iconsDirectory + "addFileSmall.png"));
		addDirectoryMenuItem = new JMenuItem(langM.getLangText("menuBarAddDirectoryMenuItem"), new ImageIcon(iconsDirectory + "addDirectorySmall.png"));
		quitMenuItem = new JMenuItem(langM.getLangText("menuBarQuitMenuItem"), new ImageIcon(iconsDirectory + "closeSmall.png"));
		aboutMenuItem = new JMenuItem(langM.getLangText("menuBarAboutMenuItem"), new ImageIcon(iconsDirectory + "infoSmall.png"));
		recursiveCallBox = new JCheckBoxMenuItem(langM.getLangText("menuBarRecursiveBox"));
		
		/**
		 * Header part
		 */
		header = new JPanel(new FlowLayout(FlowLayout.LEFT));
		headerNewListButton = new JButton(langM.getLangText("headerNewList"), new ImageIcon(iconsDirectory + "newList.png"));
		headerAddButton = new JButton(langM.getLangText("headerAdd"), new ImageIcon(iconsDirectory + "addFile.png"));
		headerAddDirectoryButton = new JButton(langM.getLangText("headerAddDirectory"), new ImageIcon(iconsDirectory + "addDirectory.png"));
		headerRemoveButton = new JButton(langM.getLangText("headerRemove"), new ImageIcon(iconsDirectory + "remove.png"));
		
		/**
		 * Footer part
		 */
		footer = new JPanel(new GridBagLayout());
		footerProgressBar = new JProgressBar();
		footerStopButton = new JButton(new ImageIcon(iconsDirectory + "stop.png"));
		
		/**
		 * Center part
		 */
		hashModel = new JTableModel();
		hashTable = new JTable(hashModel);
		hashTable.setDefaultRenderer(Object.class, new ColorCellRenderer());
		hashTable.setSelectionForeground(Color.gray);
		hashScrollPane = new JScrollPane(hashTable);
		
		/**
		 * Fill the table with empty elements
		 */
		for(int i = 0 ; i < INITIAL_HASH_SIZE ; i++)
			addElement(new JTableElement());
		
		new DropTarget(hashTable, this);
		
		fileChooser = new JFileChooser(System.getProperty("user.dir"));
	}
	
	/**
	 * Initialize the elements
	 */
	public void initialize() {
		
		setTitle(programTitle);
		setSize(optM.getInteger("window_width", WINDOW_WIDTH), optM.getInteger("window_height", WINDOW_HEIGHT));
		setLocation(optM.getInteger("window_x", 0), optM.getInteger("window_y", 0));
		setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		if(optM.getBoolean("window_maximized", false))
			setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		fileChooser.setMultiSelectionEnabled(true);
		recursiveCallBox.setSelected(true);
		
		/**
		 * Window logo
		 */
		ArrayList<Image> img = new ArrayList<Image>();
		img.add(new ImageIcon(logoDirectory + "128.png").getImage());
		img.add(new ImageIcon(logoDirectory + "96.png").getImage());
		img.add(new ImageIcon(logoDirectory + "64.png").getImage());
		img.add(new ImageIcon(logoDirectory + "48.png").getImage());
		img.add(new ImageIcon(logoDirectory + "32.png").getImage());
		setIconImages(img);
		
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu(langM.getLangText("menuBarFileMenu"));
		JMenu optionsMenu = new JMenu(langM.getLangText("menuBarOptionsMenu"));
		JMenu informationMenu = new JMenu(langM.getLangText("menuBarInformationMenu"));
		fileMenu.add(newListMenuItem);
		fileMenu.add(addFilesMenuItem);
		fileMenu.add(addDirectoryMenuItem);
		fileMenu.add(quitMenuItem);
		optionsMenu.add(recursiveCallBox);
		informationMenu.add(aboutMenuItem);
		menuBar.add(fileMenu);
		menuBar.add(optionsMenu);
		menuBar.add(informationMenu);
		setJMenuBar(menuBar);
		
		/**
		 * Header part
		 */
		headerNewListButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		headerNewListButton.setHorizontalTextPosition(SwingConstants.CENTER);
		headerAddButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		headerAddButton.setHorizontalTextPosition(SwingConstants.CENTER);
		headerAddDirectoryButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		headerAddDirectoryButton.setHorizontalTextPosition(SwingConstants.CENTER);
		headerRemoveButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		headerRemoveButton.setHorizontalTextPosition(SwingConstants.CENTER);
		
		header.add(headerNewListButton);
		header.add(headerAddButton);
		header.add(headerAddDirectoryButton);
		header.add(headerRemoveButton);
		
		/**
		 * Footer part
		 */
		footer.setPreferredSize(new Dimension(1, 40));
		footerProgressBar.setPreferredSize(new Dimension(WINDOW_WIDTH / 2, 15));
		footerProgressBar.setMaximum(100);
		
		footerStopButton.setToolTipText(langM.getLangText("footerStop"));
		footerStopButton.setBorder(new EmptyBorder(5, 5, 5, 5));
		footerStopButton.setContentAreaFilled(false);
		footerStopButton.setFocusable(false);
		footerStopButton.setBorderPainted(false);
		footerStopButton.setFocusPainted(false);
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.weightx = 0;
		footer.add(footerProgressBar);
		
		gbc.gridx = 1;
		gbc.weightx = 1;
		footer.add(footerStopButton);
		
		((JLabel)hashTable.getDefaultRenderer(Object.class)).setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		hashTable.getColumnModel().getColumn(1).setMaxWidth(230);
		hashTable.getColumnModel().getColumn(1).setPreferredWidth(230);
		hashTable.getTableHeader().setReorderingAllowed(false);
		
		setLayout(new BorderLayout());
		
		add(header, BorderLayout.NORTH);
		add(hashScrollPane, BorderLayout.CENTER);
		add(footer, BorderLayout.SOUTH);
		
		setInProgress(false);
	}
	
	/**
	 * Add the listeners
	 */
	public void addListeners() {
		
		addWindowListener(this);
		addComponentListener(this);
		addFilesMenuItem.addActionListener(this);
		newListMenuItem.addActionListener(this);
		quitMenuItem.addActionListener(this);
		aboutMenuItem.addActionListener(this);
		
		headerNewListButton.addActionListener(this);
		headerAddButton.addActionListener(this);
		headerAddDirectoryButton.addActionListener(this);
		headerRemoveButton.addActionListener(this);
		footerStopButton.addActionListener(this);
		hashTable.getSelectionModel().addListSelectionListener(this);
	}
	
	/**
	 * Change the frame subtitle
	 * @param subtitle The new subtitle
	 */
	public void addSubtitle(String subtitle) {
		
		setTitle(programTitle + " - " + subtitle);
	}
	
	/**
	 * Remove the frame subtitle
	 */
	public void removeSubtitle() {
		
		setTitle(programTitle);
	}
	
	/**
	 * Add an element to the table
	 * @param e A new element
	 */
	public void addElement(Object e) {
		
		hashModel.addElement((JTableElement)e);
	}
	
	/**
	 * Clear the table
	 */
	public void clearHash() {
		
		hashModel.clear();
		
		for(int i = 0 ; i < INITIAL_HASH_SIZE ; i++)
			addElement(new JTableElement());
	}
	
	/**
	 * Close the program
	 */
	public void closeProgram() {
		
		optM.setValueBoolean("window_maximized", (getExtendedState() & JFrame.MAXIMIZED_BOTH) != 0);
		optM.save();
		controller.onProgramExit();
		System.exit(0);
	}
	
	/**
	 * Set the progression value
	 * @param progression The progression value
	 */
	public void setProgression(int progression) {
		
		footerProgressBar.setValue(progression);
	}
	
	/**
	 * Set the progress state
	 * @param value True if a task is in progress
	 */
	public void setInProgress(boolean value) {
		
		footerProgressBar.setVisible(value);
		footerStopButton.setVisible(value);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == headerAddButton || e.getSource() == addFilesMenuItem) {
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			int returnVal = fileChooser.showOpenDialog(this);

	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	        	File[] f = fileChooser.getSelectedFiles();
	        	for(int i = 0 ; i < f.length ; i++)
	        		controller.onNewAddedFile(f[i].getAbsolutePath());
	        }
		}
		else {
			if(e.getSource() == headerAddDirectoryButton || e.getSource() == addDirectoryMenuItem) {
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = fileChooser.showOpenDialog(this);

		        if (returnVal == JFileChooser.APPROVE_OPTION) {
		        	File[] f = fileChooser.getSelectedFiles();
		        	for(int i = 0 ; i < f.length ; i++)
		        		controller.onNewAddedFile(f[i].getAbsolutePath());
		        }
			}
			else {
				if(e.getSource() == headerRemoveButton) {
					hashModel.removeElements(selectionStart, selectionEnd);
					
					if(hashModel.getRowCount() < INITIAL_HASH_SIZE) {
						for(int i = 0 ; i < INITIAL_HASH_SIZE - hashModel.getRowCount() ; i++)
							addElement(new JTableElement());
					}
				}
				else {
					if(e.getSource() == headerNewListButton) {
						clearHash();
					}
					else {
						if(e.getSource() == footerStopButton) {
							controller.onPendingWorkCanceled();
						}
						else {
							if(e.getSource() == newListMenuItem) {
								clearHash();
							}
							else {
								if(e.getSource() == quitMenuItem) {
									closeProgram();
								}
								else {
									if(e.getSource() == aboutMenuItem) {
										JOptionPane.showMessageDialog(this, programTitle + " " + langM.getLangText("aboutVersion") + " " + programVersion + "\n2012 - Stephane Baudoux\nhttp://www.stephane-baudoux.com", langM.getLangText("aboutInformation"), JOptionPane.INFORMATION_MESSAGE);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void dragEnter(DropTargetDragEvent e) {}

	@Override
	public void dragExit(DropTargetEvent e) {}

	@Override
	public void dragOver(DropTargetDragEvent e) {}

	@Override
	public void drop(DropTargetDropEvent e) {
		
		try {
			Transferable tr = e.getTransferable();
			DataFlavor[] flavors = tr.getTransferDataFlavors();
			for(int i = 0; i < flavors.length; i++) {
				if (flavors[i].isFlavorJavaFileListType()) {
					e.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);

					@SuppressWarnings("rawtypes")
					List list = (List)tr.getTransferData(flavors[i]);
					for(int j = 0 ; j < list.size() ; j++) {
						controller.onNewAddedFile(list.get(j).toString());
					}
					e.dropComplete(true);
					return;
				}
			}
		}
		catch (Exception e1) {}
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent e) {}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		
		selectionStart = e.getFirstIndex();
		selectionEnd = e.getLastIndex();
	}
	
	/**
	 * Check if the recursive call is enabled
	 * @return True if the recursive call is enabled
	 */
	public boolean isRecursiveCallChecked() {
		
		return recursiveCallBox.isSelected();
	}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowClosed(WindowEvent e) {}

	@Override
	public void windowClosing(WindowEvent e) 
	{
		closeProgram();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowOpened(WindowEvent e) {}

	@Override
	public void componentHidden(ComponentEvent e) {}

	@Override
	public void componentMoved(ComponentEvent e) {
		
		if((getExtendedState() & JFrame.MAXIMIZED_BOTH) == 0) {
			optM.setValueInteger("window_x", getX());
			optM.setValueInteger("window_y", getY());
		}
	}

	@Override
	public void componentResized(ComponentEvent e) {
		
		if((getExtendedState() & JFrame.MAXIMIZED_BOTH) == 0) {
			optM.setValueInteger("window_width", getWidth());
			optM.setValueInteger("window_height", getHeight());
		}
	}

	@Override
	public void componentShown(ComponentEvent e) {}
}
