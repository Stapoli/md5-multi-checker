/*
==============================================================================================================================================================

www.sourceforge.net/projects/md5multichecker
Copyright Stephane Baudoux (www.stephane-baudoux.com)

This file is part of MD5 Multi-Checker.

SiniP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MD5 Multi-Checker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MD5 Multi-Checker.  If not, see <http://www.gnu.org/licenses/>.

==============================================================================================================================================================
*/

package tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * 
 * @author Stephane Baudoux
 * The options manager
 */
public class OptionsManager {
	
	/**
	 * The program options file
	 */
	private static final String optionsFilename = "resources/options.dat";
	
	/**
	 * The manager instance
	 */
	private static OptionsManager instance;
	
	/**
	 * The properties file
	 */
	private static Properties options;
	
	/**
	 * Constructor
	 */
	private OptionsManager() {
		
		options = new Properties();
		try {
			options.load(new FileReader(new File(optionsFilename)));
		} 
		catch (FileNotFoundException e) {} 
		catch (IOException e) {}
	}
	
	/**
	 * Get the class instance
	 * @return The class instance
	 */
	public static OptionsManager getInstance() {
		
		if(instance == null)
			instance = new OptionsManager();
		return instance;
	}
	
	/**
	 * Get a value as a string
	 * @param key The key
	 * @param defaultValue The default value
	 * @return The value
	 */
	public String getString(String key, String defaultValue) {
		
		return options.getProperty(key, defaultValue);
	}
	
	/**
	 * Get a value as an integer
	 * @param key The key
	 * @param defaultValue The default value
	 * @return The value
	 */
	public int getInteger(String key, int defaultValue) {
		
		String tmp = options.getProperty(key);
		
		if(tmp == null)
			return defaultValue;
		
		try {
			return Integer.parseInt(tmp);
		}
		catch(NumberFormatException e) {
			return defaultValue;
		}
	}
	
	/**
	 * Get a value as a float
	 * @param key The key
	 * @param defaultValue The default value
	 * @return The value
	 */
	public float getFloat(String key, float defaultValue) {
		
		String tmp = options.getProperty(key);
		
		if(tmp == null)
			return defaultValue;
		
		try {
			return Float.parseFloat(tmp);
		}
		catch(NumberFormatException e) {
			return defaultValue;
		}
	}
	
	/**
	 * Get a value as a boolean
	 * @param key The key
	 * @param defaultValue The default value
	 * @return The value
	 */
	public boolean getBoolean(String key, boolean defaultValue) {
		String tmp = options.getProperty(key);
		
		if(tmp == null)
			return defaultValue;
		
		try {
			return Boolean.parseBoolean(tmp);
		}
		catch(NumberFormatException e) {
			return defaultValue;
		}
	}
	
	/**
	 * Set a string value
	 * @param key The key
	 * @param value The value
	 */
	public void setValueString(String key, String value) {
		
		options.setProperty(key, value);
	}
	
	/**
	 * Set a boolean value
	 * @param key The key
	 * @param value The value
	 */
	public void setValueBoolean(String key, boolean value) {
		
		try {
			options.setProperty(key, String.valueOf(value));
		}
		catch(NumberFormatException e) {}
	}
	
	/**
	 * Set an integer value
	 * @param key The key
	 * @param value The value
	 */
	public void setValueInteger(String key, int value) {
		
		try {
			options.setProperty(key, String.valueOf(value));
		}
		catch(NumberFormatException e) {}
	}
	
	/**
	 * Set a float value
	 * @param key The key
	 * @param value The value
	 */
	public void setValueFloat(String key, float value) {
		
		try {
			options.setProperty(key, String.valueOf(value));
		}
		catch(NumberFormatException e) {}
	}
	
	/**
	 * Save the options
	 */
	public void save() {
		
		try  {
			options.store(new FileWriter(new File(optionsFilename)), "");
		} 
		catch (IOException e) {}
	}
}
