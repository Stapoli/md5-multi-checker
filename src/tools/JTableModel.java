/*
==============================================================================================================================================================

www.sourceforge.net/projects/md5multichecker
Copyright Stephane Baudoux (www.stephane-baudoux.com)

This file is part of MD5 Multi-Checker.

SiniP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MD5 Multi-Checker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MD5 Multi-Checker.  If not, see <http://www.gnu.org/licenses/>.

==============================================================================================================================================================
*/

package tools;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

/**
 * 
 * @author Stephane Baudoux
 * The table model
 *
 */
public class JTableModel extends AbstractTableModel {
	
	/**
	 * The language manager
	 */
	private LangManager langM = LangManager.getInstance();
	
	/**
	 * The data
	 */
	private ArrayList<JTableElement> data = new ArrayList<JTableElement>();
	
	/**
	 * The header inforamtion
	 */
	private final String[] header = {langM.getLangText("hashTableColumn1"), langM.getLangText("hashTableColumn2")};
	
	private static final long serialVersionUID = 1L;

	@Override
	public int getColumnCount() {
		
		return header.length;
	}

	@Override
	public int getRowCount() {
		
		return data.size();
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		
        return header[columnIndex];
    }

	@Override
	public Object getValueAt(int row, int column) {
		
		switch(column)
		{
		case 0:
			return data.get(row).getFileName();
			
		case 1:
			return data.get(row).getMd5();
			
		default:
			return null;
		}
	}

	/**
	 * Add an element
	 * @param e The element to add
	 */
	public void addElement(JTableElement e) {
		
		boolean replaced = false;
		for(int i = 0 ; i < data.size() && !replaced ; i++) {
			if(data.get(i).isEmpty() && !e.isEmpty()) {
				data.get(i).setElement(e.getFileName(), e.getMd5());
				fireTableDataChanged();
				replaced = true;
			}
		}
		
		if(!replaced) {
	        data.add(e);
	        fireTableRowsInserted(data.size() -1, data.size() -1);
		}
    }

	/**
	 * Remove an element
	 * @param rowIndex The index of the element
	 */
    public void removeElement(int rowIndex) {
    	
    	data.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
    
    /**
     * Remove a set of elements
     * @param rowIndexStart The start index
     * @param rowIndexEnd The end index
     */
    public void removeElements(int rowIndexStart, int rowIndexEnd) {
    	
    	int size = rowIndexEnd - rowIndexStart;
    	while (size >= 0) {
    		data.remove(rowIndexStart);
    		size--;
    	}
    	fireTableDataChanged();
    }
    
    /**
     * Clear the table
     */
    public void clear() {
    	
    	data.clear();
    	fireTableDataChanged();
    }
}
