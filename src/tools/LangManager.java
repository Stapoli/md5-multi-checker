/*
==============================================================================================================================================================

www.sourceforge.net/projects/md5multichecker
Copyright Stephane Baudoux (www.stephane-baudoux.com)

This file is part of MD5 Multi-Checker.

SiniP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MD5 Multi-Checker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MD5 Multi-Checker.  If not, see <http://www.gnu.org/licenses/>.

==============================================================================================================================================================
*/

package tools;

import java.io.FileReader;
import java.util.Locale;
import java.util.Properties;

/**
 * 
 * @author Stephane Baudoux
 * The language manager.
 * Manages the language files.
 */
public class LangManager {
	
	/**
	 * The language directory
	 */
	private static String langDirectory = "resources/lang/";
	
	/**
	 * The instance
	 */
	private static LangManager instance;
	
	/**
	 * The properties file
	 */
	private static Properties langText;
	
	/**
	 * Constructor
	 */
	private LangManager() {
		
		super();
		selectLangPack();
	}
	
	/**
	 * Select a language file depending on the OS language
	 * Use the English one if no language found for the is language
	 */
	public void selectLangPack() {
		
		langText = new Properties();
		Locale local = Locale.getDefault();
		
		try {
			langText.load(new FileReader(langDirectory + "lang_" + local.getLanguage() + ".lang"));
		}
		catch(Exception e) {
			try {
				langText.load(new FileReader(langDirectory + "lang_en.lang"));
			}
			catch(Exception f){}
		}
	}
	
	/**
	 * Get the class instance
	 * @return The class instance
	 */
	public static LangManager getInstance() {
		
		if(instance == null)
			instance = new LangManager();
		return instance;
	}
	
	/**
	 * Get a language value
	 * @param key The key
	 * @return The associated value
	 */
	public String getLangText(String key) {
		
		return langText.getProperty(key);
	}
}