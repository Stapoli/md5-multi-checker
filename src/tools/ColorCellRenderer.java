/*
==============================================================================================================================================================

www.sourceforge.net/projects/md5multichecker
Copyright Stephane Baudoux (www.stephane-baudoux.com)

This file is part of MD5 Multi-Checker.

SiniP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MD5 Multi-Checker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MD5 Multi-Checker.  If not, see <http://www.gnu.org/licenses/>.

==============================================================================================================================================================
*/

package tools;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * 
 * @author Stephane Baudoux
 * The cell renderer (for the alternated color)
 *
 */
public class ColorCellRenderer extends DefaultTableCellRenderer {
	
	private static final long serialVersionUID = 1L;

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        Color color;
        if(row % 2 == 1)
        	color = new Color(0.8f, 1, 1);
        else
        	color = Color.white;
        
        if(column == 1)
        	setHorizontalAlignment(JLabel.CENTER);
        else
        	setHorizontalAlignment(JLabel.LEFT);

        setText((String)value);
        setBackground(color);

        return this;
    }
}
