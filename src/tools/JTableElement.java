/*
==============================================================================================================================================================

www.sourceforge.net/projects/md5multichecker
Copyright Stephane Baudoux (www.stephane-baudoux.com)

This file is part of MD5 Multi-Checker.

SiniP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MD5 Multi-Checker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MD5 Multi-Checker.  If not, see <http://www.gnu.org/licenses/>.

==============================================================================================================================================================
*/

package tools;

/**
 * 
 * @author Stephane Baudoux
 * Table line implementation.
 *
 */
public class JTableElement {
	
	/**
	 * The filename
	 */
	private String file;
	
	/**
	 * The md5 hash
	 */
	private String md5;
	
	/**
	 * Constructor
	 */
	public JTableElement() {
		
		super();
		file = "";
		md5 = "";
	}
	
	/**
	 * Constructor
	 * @param f The file path
	 * @param hash The md5 hash
	 */
	public JTableElement(String f, String hash) {
		
		super();
		file = f;
		md5 = hash;
	}
	
	/**
	 * Set the element
	 * @param f The file path
	 * @param hash The md5 hash
	 */
	public void setElement(String f, String hash) {
		
		file = f;
		md5 = hash;
	}
	
	/**
	 * Check if the element is empty
	 * @return True if the element is empty
	 */
	public boolean isEmpty() {
		
		return file.equals("") && md5.equals("");
	}
	
	/**
	 * Get the filename
	 * @return The file
	 */
	public String getFileName() {
		
		return file;
	}
	
	/**
	 * Get the md5 hash
	 * @return The md5 hash
	 */
	public String getMd5() {
		
		return md5;
	}
}
